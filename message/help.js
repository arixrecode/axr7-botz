const groupmenu = (prefix, salam, pushname, isOwner, Axr7, sender) => {
	return `╔═════════════════㋛︎
║┏━━「 𝗔𝗕𝗢𝗨𝗧 𝗨𝗦𝗘𝗥 」
║┃
║┣☢︎︎ ${salam} kak
║┣☢︎︎ *Nama* : ${pushname}
║┣☢︎︎ *Owner* : ${isOwner ? "true":"false"}
║┣☢︎︎ *Nomor* : ${sender.split('@')[0]}
║┗━━━━━━━━━━━━㋛︎
║┏━━「 𝗚𝗥𝗢𝗨𝗣 𝗠𝗘𝗡𝗨 」
║┃
║┣☢︎︎ ${prefix}tagall
║┣☢︎︎ ${prefix}listadmin
║┣☢︎︎ ${prefix}demote
║┣☢︎︎ ${prefix}promote
║┣☢︎︎ ${prefix}kick
║┣☢︎︎ ${prefix}add
║┣☢︎︎ ${prefix}setdesc
║┣☢︎︎ ${prefix}setnamegc
║┣☢︎︎ ${prefix}group
║┣☢︎︎ ${prefix}closetime
║┣☢︎︎ ${prefix}opentime
║┣☢︎︎ ${prefix}setppgc
║┣☢︎︎ ${prefix}leave
║┣☢︎︎ ${prefix}leavetime
║┣☢︎︎ ${prefix}linkgc
║┣☢︎︎ ${prefix}getbio
║┣☢︎︎ ${prefix}antilink
║┣☢︎︎ ${prefix}revoke
║┣☢︎︎ ${prefix}kontag
║┣☢︎︎ ${prefix}stictktag
║┣☢︎︎ ${prefix}totag
║┣☢︎︎ ${prefix}hidetag
║┣☢︎︎ ${prefix}dissapearing
║┣☢︎︎ ${prefix}notif
║┣☢︎︎ ${prefix}infoall
║┣☢︎︎ ${prefix}listonline
║┣☢︎︎ ${prefix}ownergroup
║┗━━━━「 ${Axr7.user.name} 」
╚═════════════════㋛︎
`
}

const stickermenu = (prefix, salam, pushname, isOwner, Axr7, sender) => {
	return `╔═════════════════㋛︎
║┏━━「 𝗔𝗕𝗢𝗨𝗧 𝗨𝗦𝗘𝗥 」
║┃
║┣☢︎︎ ${salam} kak
║┣☢︎︎ *Nama* : ${pushname}
║┣☢︎︎ *Owner* : ${isOwner ? "true":"false"}
║┣☢︎︎ *Nomor* : ${sender.split('@')[0]}
║┗━━━━━━━━━━━━㋛︎
║┏━━「 𝗦𝗧𝗜𝗖𝗞𝗘𝗥 𝗠𝗘𝗡𝗨 」
║┃
║┣☢︎︎ ${prefix}sticker
║┣☢︎︎ ${prefix}stickergif
║┣☢︎︎ ${prefix}take
║┣☢︎︎ ${prefix}sticker nobg
║┣☢︎︎ ${prefix}stickerwm
║┗━━━━「 ${Axr7.user.name} 」
╚═════════════════㋛︎
`
}

const ownermenu = (prefix, salam, pushname, isOwner, Axr7, sender) => {
	return `╔═════════════════㋛︎
║┏━━「 𝗔𝗕𝗢𝗨𝗧 𝗨𝗦𝗘𝗥 」
║┃
║┣☢︎︎ ${salam} kak
║┣☢︎︎ *Nama* : ${pushname}
║┣☢︎︎ *Owner* : ${isOwner ? "true":"false"}
║┣☢︎︎ *Nomor* : ${sender.split('@')[0]}
║┗━━━━━━━━━━━━㋛︎
║┏━━「 𝗢𝗪𝗡𝗘𝗥 𝗠𝗘𝗡𝗨 」
║┃
║┣☢︎︎ ${prefix}setppbot
║┣☢︎︎ ${prefix}block
║┣☢︎︎ ${prefix}unblock
║┣☢︎︎ ${prefix}bc
║┣☢︎︎ x
║┣☢︎︎ =>
║┣☢︎︎ $
║┗━━━━「 ${Axr7.user.name} 」
╚═════════════════㋛︎
`
}

const convertmenu = (prefix, salam, pushname, isOwner, Axr7, sender) => {
	return `╔═════════════════㋛︎
║┏━━「 𝗔𝗕𝗢𝗨𝗧 𝗨𝗦𝗘𝗥 」
║┃
║┣☢︎︎ ${salam} kak
║┣☢︎︎ *Nama* : ${pushname}
║┣☢︎︎ *Owner* : ${isOwner ? "true":"false"}
║┣☢︎︎ *Nomor* : ${sender.split('@')[0]}
║┗━━━━━━━━━━━━㋛︎
║┏━━「 𝗖𝗢𝗡𝗩𝗘𝗥𝗧 𝗠𝗘𝗡𝗨 」
║┃
║┣☢︎︎ ${prefix}toimg
║┣☢︎︎ ${prefix}bass
║┣☢︎︎ ${prefix}slowmo
║┣☢︎︎ ${prefix}tupai
║┣☢︎︎ ${prefix}gemok
║┣☢︎︎ ${prefix}text2speach
║┣☢︎︎ ${prefix}tomp3
║┣☢︎︎ ${prefix}reverse
║┣☢︎︎ ${prefix}slow
║┣☢︎︎ ${prefix}fast
║┗━━━━「 ${Axr7.user.name} 」
╚═════════════════㋛︎
`
}

const othermenu = (prefix, salam, pushname, isOwner, Axr7, sender) => {
	return `╔═════════════════㋛︎
║┏━━「 𝗔𝗕𝗢𝗨𝗧 𝗨𝗦𝗘𝗥 」
║┃
║┣☢︎︎ ${salam} kak
║┣☢︎︎ *Nama* : ${pushname}
║┣☢︎︎ *Owner* : ${isOwner ? "true":"false"}
║┣☢︎︎ *Nomor* : ${sender.split('@')[0]}
║┗━━━━━━━━━━━━㋛︎
║┏━━「 𝗢𝗧𝗛𝗘𝗥 𝗠𝗘𝗡𝗨 」
║┃
║┣☢︎︎ ${prefix}delete
║┣☢︎︎ ${prefix}get
║┗━━━━「 ${Axr7.user.name} 」
╚═════════════════㋛︎
`
}

const funmenu = (prefix, salam, pushname, isOwner, Axr7, sender) => {
	return `╔═════════════════㋛︎
║┏━━「 𝗔𝗕𝗢𝗨𝗧 𝗨𝗦𝗘𝗥 」
║┃
║┣☢︎︎ ${salam} kak
║┣☢︎︎ *Nama* : ${pushname}
║┣☢︎︎ *Owner* : ${isOwner ? "true":"false"}
║┣☢︎︎ *Nomor* : ${sender.split('@')[0]}
║┗━━━━━━━━━━━━㋛︎
║┏━━「 𝗙𝗨𝗡 𝗠𝗘𝗡𝗨 」
║┃
║┣☢︎︎ ${prefix}apakah
║┣☢︎︎ ${prefix}bisakah
║┣☢︎︎ ${prefix}kapankah
║┣☢︎︎ ${prefix}tagme
║┣☢︎︎ ${prefix}cantik
║┣☢︎︎ ${prefix}beban
║┣☢︎︎ ${prefix}gila
║┣☢︎︎ ${prefix}ganteng
║┗━━━━「 ${Axr7.user.name} 」
╚═════════════════㋛︎
`
}

module.exports = { funmenu, groupmenu, stickermenu, convertmenu, ownermenu, othermenu }