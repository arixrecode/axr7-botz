exports.wait = () => {
	return`Silahkan tunggu beberapa menit`
}

exports.onlyAdmin = () => {
	return `Perintah ini hanya bisa digunakan oleh admin group!`
}

exports.onlyGroup = () => {
	return `Perintah ini hanya bisa digunakan di dalam group!`
}

exports.onlyBotAdmin = () => {
	return `Perintah inu hanya bisa digunakan ketika bot menjadi admin!`
}

exports.onlyOwnerBot = () => {
	return `Petintah ini hanya bisa digunakan oleh owner bot`
}

exports.errorStick = () => {
	return `Gagal, terjadi kesalahan saat mengkonversi ke stocker, coba ulangi beberapa saat lagi!`
}

exports.groupLogger = (pushname, sender, isGroup, groupName, command) => {
	return `
☢︎︎ *Nama* : ${pushname}
☢︎︎ *No* : ${sender.split('@')[0]}
☢︎︎ *In* : ${isGroup ? `Group\n  -> *${groupName}*`:"Private"}
☢︎︎ *Command* : ${command}
`
}

exports.vn = () => {
	return `Reply vn!`
}