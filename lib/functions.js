const fetch = require('node-fetch')
const cfonts = require('cfonts')
const Crypto = require('crypto')
const axios = require('axios')
const moment = require('moment-timezone')
const fs = require('fs')	
const { spawn } = require("child_process")

const banner = cfonts.render(('loli killers|wabot|termux'), {
	font: 'pallet',
	align: 'center',
	colors: 'candy',
	background: 'transparent',
	letterSpacing: 1,
	lineHeight: 1,
	space: true,
	maxLength: 0,
	gradient: ['yellow', 'cyan'],
	independentGradient: false,
	transitionGradient: false,
	env: 'node'
});

const getGroupAdmins = (participants) => {
	admins = []
	for (let x of participants) {
		x.isAdmin ? admins.push(x.jid) : ''
	}
	return admins
}

const battery = {
  persen: "" || "Tidak terdeteksi",
  charger: "" || false
}

const time = moment().tz('Asia/Jakarta').format('HH:mm:ss')
if (time < "23:59:00") {
	var salam = 'Selamat Malam'
}
if (time < "19:00:00") {
	var salam = 'Selamat Malam'
}
if (time < "18:00:00") {
	var salam = 'Selamat Sore'
}
if (time < "15:00:00") {
	var salam = 'Selamat Siang'
}
if (time < "11:00:00") {
	var salam = 'Selamat Pagi'
}
if (time < "06:00:00") {
	var salam = 'Selamat Pagi'
}

function tanggal(){
  myMonths = ["ᴊᴀɴᴜᴀʀɪ","ғᴇʙʀᴜᴀʀɪ","ᴍᴀʀᴇᴛ","ᴀᴘʀɪʟ","ᴍᴇɪ","ᴊᴜɴɪ","ᴊᴜʟɪ","ᴀɢᴜsᴛᴜs","sᴇᴘᴛᴇᴍʙᴇʀ","ᴏᴋᴛᴏʙᴇʀ","ɴᴏᴠᴇᴍʙᴇʀ","ᴅᴇsᴇᴍʙᴇʀ"];
	myDays = ['ᴍɪɴɢɢᴜ','sᴇɴɪɴ','sᴇʟᴀsᴀ','ʀᴀʙᴜ','ᴋᴀᴍɪs','ᴊᴜᴍᴀᴛ','sᴀʙᴛᴜ'];
	var tgl = new Date();
	var day = tgl.getDate();
	bulan = tgl.getMonth();
	var thissDay = tgl.getDay(),
	thisDay = myDays[thissDay];
	var yy = tgl.getYear();
	var year = (yy < 1000) ? yy + 1900 : yy;
	return `${thisDay}, ${day} - ${myMonths[bulan]} - ${year}`;
}

const kyun = (seconds) =>{
    function pad(s) {
        return (s < 10 ? '0' : '') + s;
    }
    var hours = Math.floor(seconds / (60 * 60));
    var minutes = Math.floor(seconds % (60 * 60) / 60);
    var seconds = Math.floor(seconds % 60);
    return `${pad(hours)} ᴊᴀᴍ - ${pad(minutes)} ᴍᴇɴɪᴛ -  ${pad(seconds)} ᴅᴇᴛɪᴋ`
}

const getRandom = (ext) => {
	return `${Math.floor(Math.random() * 10000)}${ext}`
}

function addMetadata(packname, author) {	
	if (!packname) packname = 'Axr7-Botz'; if (!author) author = 'AriExRecode7';	
	author = author.replace(/[^a-zA-Z0-9]/g, '');
	let name = `${author}_${packname}`
	if (fs.existsSync(`./src/stickers/${name}.exif`)) return `./src/stickers/${name}.exif`
	const json = {
		"sticker-pack-name": packname,
		"sticker-pack-publisher": author,
	}
	const littleEndian = Buffer.from([0x49, 0x49, 0x2A, 0x00, 0x08, 0x00, 0x00, 0x00, 0x01, 0x00, 0x41, 0x57, 0x07, 0x00])	
	const bytes = [0x00, 0x00, 0x16, 0x00, 0x00, 0x00]	
	let len = JSON.stringify(json).length
	let last
	if (len > 256) {
		len = len - 256	
		bytes.unshift(0x01)	
	} else {
		bytes.unshift(0x00)	
	}
	if (len < 16) {
		last = len.toString(16)
		last = "0" + len
	} else {
		last = len.toString(16)	
	}
	const buf2 = Buffer.from(last, "hex")
	const buf3 = Buffer.from(bytes)
	const buf4 = Buffer.from(JSON.stringify(json))
	const buffer = Buffer.concat([littleEndian, buf2, buf3, buf4])
	fs.writeFile(`./src/stickers/${name}.exif`, buffer, (err) => {
		return `./src/stickers/${name}.exif`
	})
}

const createExif = (pack, auth) =>{
    const code = [0x00,0x00,0x16,0x00,0x00,0x00]
    const exif = {"sticker-pack-id": "com.client.tech", "sticker-pack-name": pack, "sticker-pack-publisher": auth, "android-app-store-link": "https://play.google.com/store/apps/details?id=com.termux", "ios-app-store-link": "https://itunes.apple.com/app/sticker-maker-studio/id1443326857"}
    let len = JSON.stringify(exif).length
    if (len > 256) {
        len = len - 256
        code.unshift(0x01)
    } else {
        code.unshift(0x00)
    }
    if (len < 16) {
        len = len.toString(16)
        len = "0" + len
    } else {
        len = len.toString(16)
    }
    //len = len < 16 ? `0${len.toString(16)}` : len.toString(16)
    const _ = Buffer.from([0x49, 0x49, 0x2A, 0x00, 0x08, 0x00, 0x00, 0x00, 0x01, 0x00, 0x41, 0x57, 0x07, 0x00]);
    const __ = Buffer.from(len, "hex")
    const ___ = Buffer.from(code)
    const ____ = Buffer.from(JSON.stringify(exif))
    fs.writeFileSync('./src/stickers/data.exif', Buffer.concat([_, __, ___, ____]), function (err) {
        console.log(err)
        if (err) return console.error(err)
        return `./src/stickers/data.exif`
    })

}

const getBuffer = async (url) => {
	const res = await fetch(url, {headers: { 'User-Agent': 'okhttp/4.5.0'}, method: 'GET' })
	const anu = fs.readFileSync('./img/error.jpeg')
	if (!res.ok) return { type: 'image/jpeg', result: anu }
	const buff = await res.buffer()
	if (buff)
	return { type: res.headers.get('content-type'), result: buff }
}

const modStick = (media, Axr7, Axr, from) => {
    out = getRandom('.webp')
    try {
        console.info(media)
        spawn('webpmux', ['-set','exif', './src/stickers/data.exif', media, '-o', out])
        .on('exit', () => {
            Axr7.sendMessage(from, fs.readFileSync(out), 'stickerMessage', {quoted: Axr})
            fs.unlinkSync(out)
            fs.unlinkSync(media)
        })
    } catch (e) {
        console.log(e)
        Axr7.sendMessage(from, 'Terjadi keslahan', 'conversation', { quoted: Axr })
        fs.unlinkSync(media)
    }
}

module.exports = { banner, modStick, getBuffer, getGroupAdmins, battery, salam, tanggal, kyun, getRandom, addMetadata, createExif}