const {
	WAConnection,
	Mimetype,
	MessageType,
 } = require('@adiwajshing/baileys')
const { banner } = require('./lib/functions')
const warn = require('console-warn')
const info = require('console-info')
const error = require('console-error')
const fs = require('fs')
const setting = JSON.parse(fs.readFileSync('./config/settings.json'))
const { exit } = require('process')
const os = require('os')
const axios = require('axios')

require('./main.js')
require('./message/help.js')
nocache('./main.js', module => console.log(`${module} is now updated!`))
nocache('./message/help.js', module => console.log(`${module} is now updated!`))

const starts = async (Axr7 = new WAConnection()) => {
	Axr7.logger.level = 'warn'
	Axr7.version = [2, 2123, 8]
	Axr7.browserDescription = ['Axr7', 'Chrome', '3.0']
	console.log(banner.string)
	Axr7.on('qr', () => {
		console.info('Kode qr sudah siap untuk di scan!')
	})
	
	fs.existsSync('./recode.json') && Axr7.loadAuthInfo('./recode.json')
	if(setting.keyscript !== "Axr7Solo") {
		console.error('keyscript is empty, please check file settings.json')
		exit()
	}
	
	Axr7.on('connecting', () => {
		console.warn('Openned connection to WhatsApp Web')
	})
	
		Axr7.on('open', () => {
		console.info('Connected to WhatsApp Web')
		console.log(`┏━━━[ ${Axr7.user.name} ]━[]`)
		console.log(`┃`)
		console.log(`┠⧐ Server : ${Axr7.browserDescription[0]}`)
		console.log(`┠⧐ Browser : ${Axr7.browserDescription[1]}`)
		console.log(`┠⧐ Version : ${Axr7.browserDescription[2]}`)
		console.log(`┠⧐ Platform : ${os.platform()}`)
		console.log(`┠⧐ Hostname : ${os.hostname()}`)
		console.log(`┠⧐ Device : ${Axr7.user.phone.device_manufacturer}`)
		console.log(`┠⧐ Model : ${Axr7.user.phone.device_model}`)
		console.log(`┠⧐ Wa Version : ${Axr7.user.phone.wa_version}`)
		console.log(`┗━━━━━━━━━━━━━━━━━━[]`)
	})
	
	await Axr7.connect({timeoutMs: 30*2000})
	fs.writeFileSync('./recode.json', JSON.stringify(Axr7.base64EncodedAuthInfo(), null, '\t'))
	
	Axr7.on('chat-update', async (message) => {
		require('./main.js')(Axr7, message)
	})
	
	Axr7.on("CB:action,,battery", async (json) => {
		require('./main.js')(Axr7, json)
	})
	
	Axr7.on('chats-received', async ({ hasNewChats }) => {
		console.info(`⧐ Total Chats : ${Axr7.chats.length} chat`)
		const unread = await Axr7.loadAllUnreadMessages ()
		console.info ("⧐ Unread : " + unread.length + " unread message")
	})
	
	Axr7.on('contacts-received', () => {
		console.info('⧐ Contacts : ' + Object.keys(Axr7.contacts).length + ' contacts')
		console.error('Hello ' + Axr7.user.name + ' (' + Axr7.user.jid + ')')    
	})
	
	Axr7.on('group-participants-update', async (gace) => {
		loli = { key: {fromMe: false,participant: "0@s.whatsapp.net", remoteJid: "0@s.whatsapp.net"}, message: {"groupInviteMessage": {"groupJid": "6285785445412-1631620219@g.us", "inviteCode": "mememteeeekkeke","groupName": "Axr7.net", "caption": `Ari x Recode 7`, 'jpegThumbnail': fs.readFileSync(`./img/${setting.fakethumb.thumb2}`)}}}
		console.log(gace)
		if (gace.action == 'add') {
			const mdata = await Axr7.groupMetadata(gace.jid)
			iki_nom = gace.participants[0]
			textwel = `@${iki_nom.split('@')[0]} Intro dulu!!`
			Axr7.sendMessage(mdata.id, textwel, MessageType.text, {quoted: loli, contextInfo: { "mentionedJid" : [iki_nom], "forwardingScore":999,"isForwarded":true},sendEphemeral: true})
		} else if (gace.action == 'remove') {
			const mdata = await Axr7.groupMetadata(gace.jid)
			iki_nom = gace.participants[0]
			textwel = `@${iki_nom.split('@')[0]} contoh anak ngent*d`
			Axr7.sendMessage(mdata.id, textwel, MessageType.text, {quoted: loli, contextInfo: { "mentionedJid" : [iki_nom], "forwardingScore":999,"isForwarded":true},sendEphemeral: true})
		} else if (gace.action == 'promote') {
			const mdata = await Axr7.groupMetadata(gace.jid)
			iki_nom = gace.participants[0]
			iki_tek = `╭══─ᕕ *Promote Detcted* ᕗ
├☢︎︎ *Nomor* : ${iki_nom.replace('@s.whatsapp.net', '')}
├☢︎︎ *User* : @${iki_nom.split('@')[0]}
├☢︎︎ *Group* : ${mdata.subject}
╰────ᕕ
`
			Axr7.sendMessage(mdata.id, iki_tek, MessageType.text, {contextInfo: {mentionedJid: [iki_nom],"forwardingScore":999,"isForwarded":true},sendEphemeral: true , quoted : loli})
		} else if (gace.action == 'demote') {
			const mdata = await Axr7.groupMetadata(gace.jid)
			iki_nom = gace.participants[0]
			iki_tek = `╭══─ᕕ *Demote Detcted* ᕗ
├☢︎︎ *Nomor* : ${iki_nom.replace('@s.whatsapp.net', '')}
├☢︎︎ *User* : @${iki_nom.split('@')[0]}
├☢︎︎ *Group* : ${mdata.subject}
╰────ᕕ
`
			try {
				Axr7.sendMessage(mdata.id, iki_tek, MessageType.text, {contextInfo: {mentionedJid: [iki_nom],"forwardingScore":999,"isForwarded":true},sendEphemeral: true  , quoted: loli})
			} catch (e) {
				console.error('Error : %s', e)
			}
		}
	})
	
	Axr7.on('group-update', async (gace) => {
		loli = { key: {fromMe: false, participant: "0@s.whatsapp.net", remoteJid: "0@s.whatsapp.net"}, message: {"groupInviteMessage": {"groupJid": "6285785445412-1631620219@g.us", "inviteCode": "mememteeeekkeke","groupName": "Axr7.net", "caption": `Ari x Recode 7`, 'jpegThumbnail': fs.readFileSync(`./img/${setting.fakethumb.thumb2}`)}}}
		metdata = await Axr7.groupMetadata(gace.jid)
		if (gace.announce == 'false') {
			iki_tek = `*ᕕ Group Open ᕗ*\n\nGroup telah dibuka oleh admin`
			Axr7.sendMessage(metdata.id, iki_tek, MessageType.text, {quoted: loli})
			console.warn(`ᕕ Group Open ᕗ In ${metdata.subject}`)
		} else if (gace.announce == 'true') {
			iki_tek = `*ᕕ Group Close ᕗ*\n\nGroup telah ditutup oleh admin`
			Axr7.sendMessage(metdata.id, iki_tek, MessageType.text, {quoted: loli})
			console.warn(`ᕕ Group Close ᕗ In ${metdata.subject}`)
		} else if (!gace.desc == '') {
			iki_tag = gace.descOwner.split('@')[0] + '@s.whatsapp.net'
			iki_tek = `*ᕕ Load Desc Group ᕗ*\n\nDeskripsi Group telah diubah oleh Admin @${gace.descOwner.split('@')[0]}\n\n☢︎︎ New Desc : ${gace.desc}`
			Axr7.sendMessage(metdata.id, iki_tek, MessageType.text, {contextInfo: {"mentionedJid": [iki_tag]}, quoted: loli})
			console.warn(`ᕕ Load Desc Group ᕗ In ${metdata.subject}`)
		} else if (gace.restrict == 'false') {
			iki_tek = `ᕕ Load Desc Group ᕗ\n\nEdit Group info telah dibuka untuk member`
			Axr7.sendMessage(metdata.id, iki_tek, MessageType.text, {quoted: loli})
			console.warn(`ᕕ Load Desc Group ᕗ In ${metdata.subject}`)
		} else if (gace.restrict == 'true') {
			iki_tek = `ᕕ Load Desc Group ᕗ\n\nEdit Group info telah ditutup untuk member`
			Axr7.sendMessage(metdata.id, iki_tek, MessageType.text, {quoted: loli})
			console.warn(`ᕕ Load Desc Group ᕗ In ${metdata.subject}`)
		}
	})
}

function nocache(module, cb = () => { }) {
	console.info('File', `'${module}'`, 'loaded success')
	fs.watchFile(require.resolve(module), async () => {
		await uncache(require.resolve(module))
		cb(module)
	})
}

function uncache(module = '.') {
	return new Promise((resolve, reject) => {
		try {
			delete require.cache[require.resolve(module)]
			resolve()
		} catch (e) {
			reject(e)
		}
	})
}
	
starts()